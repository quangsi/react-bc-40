import React, { Component } from 'react';
import ItemPhone from './ItemPhone';

export default class ListPhone extends Component {
  renderListPhone = () => {
    return this.props.phoneArr.map((item) => {
      return (
        <ItemPhone
          handleChangeDetail={this.props.handleChangeDetail}
          key={item.tenSP}
          data={item}
        />
      );
    });
  };
  render() {
    console.log('props', this.props);
    return <div className="row">{this.renderListPhone()}</div>;
  }
}
