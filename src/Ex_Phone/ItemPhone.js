import React, { Component } from 'react';

export default class ItemPhone extends Component {
  render() {
    let { hinhAnh, tenSP, giaBan } = this.props.data;
    return (
      <div className="card border-primary col-4 ">
        <img
          className="card-img-top"
          src={hinhAnh}
          style={{ height: 300 }}
          alt
        />
        <div className="card-body">
          <h4 className="card-title">{tenSP}</h4>
          <p className="card-text">{giaBan}</p>
          <button
            onClick={() => {
              this.props.handleChangeDetail(this.props.data);
            }}
            className="btn btn-secondary text-white"
          >
            Xem chi tiết
          </button>
        </div>
      </div>
    );
  }
}
