import React, { Component } from 'react';

export default class DetailPhone extends Component {
  render() {
    let {
      tenSP,
      manHinh,
      heDieuHanh,
      cameraTruoc,
      cameraSau,
      ram,
      rom,
      giaBan,
      hinhAnh,
    } = this.props.data;
    return (
      <div className="row">
        <img src={hinhAnh} alt="" className="col-4" />
        <div className="col-8">
          <p>{tenSP}</p>
          <p>{manHinh}</p>
          <p>{heDieuHanh}</p>
          <p>{cameraTruoc}</p>
          <p>{cameraSau}</p>
          <p>{ram}</p>
          <p>{rom}</p>
          <p>{giaBan}</p>
        </div>
      </div>
    );
  }
}
