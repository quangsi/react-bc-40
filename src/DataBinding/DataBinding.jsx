import React, { Component } from 'react';

export default class DataBinding extends Component {
  username = 'Alice';
  renderButton = () => {
    return <button className="btn btn-danger">Logout</button>;
  };
  render() {
    let title = 'Username: ';
    console.log(this.username);
    return (
      <div>
        {/* {} data binding */}
        <h1>DataBinding</h1>
        <h2>{title}</h2>
        <h2>{this.username} </h2>
        {this.renderButton()}
      </div>
    );
  }
}
