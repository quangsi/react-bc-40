import "./App.css";
import Ex_Shoe_Shop_Redux from "./Ex_Shoe_Shop_Redux/Ex_Shoe_Shop_Redux";
// import DemoClassComponent from './DemoComponent/DemoClassComponent';
function App() {
  return (
    // jsx (html + js)
    <div className="App">
      {/* ---- buổi 1 ---- */}
      {/* <DemoClassComponent></DemoClassComponent> */}
      {/* <DemoFunctionComponent /> */}
      {/* <DataBinding /> */}
      {/* <Ex_Layout /> */}
      {/* <Ex_Demo /> */}

      {/* --- buổi 2 ---- */}
      {/* <EventHandling /> */}
      {/* <DemoState /> */}
      {/* <RenderingWithMap /> */}
      {/* <Ex_Car_Color /> */}

      {/* --- buổi 3 --- */}
      {/* <DemoProp /> */}
      {/* <Ex_Shoe_Shop> Cyber Shoe Shop </Ex_Shoe_Shop> */}
      {/* <Ex_Phone /> */}
      {/*  -- redux -- */}
      {/* <DemoRedux_Mini /> */}
      <Ex_Shoe_Shop_Redux />
    </div>
  );
}
/*  */
export default App;
