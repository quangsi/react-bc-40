import React, { Component, Fragment } from 'react';
import styles from './menu.module.css';
export default class Menu extends Component {
  render() {
    // <> </> : thẻ rỗng, sẽ được remove khi render lên giao diện
    return (
      <Fragment>
        <h2 className={styles.title}>Menu :</h2>
        <button
          className="btn 
        btn-success"
        >
          Login
        </button>
        <button className="btn btn-danger">Logout</button>
      </Fragment>
    );
  }
}
//   <>
//     <button className="btn btn-success">Login</button>
//     <button className="btn btn-danger">Logout</button>
//   </>
//   <Fragment> : tương tự thẻ rỗng
