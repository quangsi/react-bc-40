import React, { Component } from 'react';
import Content from './Content';
import Footer from './Footer';
import Header from './Header';
import Menu from './Menu';
import Navigate from './Navigate';

export default class Ex_Demo extends Component {
  render() {
    return (
      <div>
        <Header />
        <div id="demo">
          <Menu />
          <Menu />
          <Menu />
          <Menu />
        </div>
        <div className="row">
          <div className="col-6 p-0">
            <Navigate />
          </div>
          <div className="col-6 p-0">
            <Content />
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}
