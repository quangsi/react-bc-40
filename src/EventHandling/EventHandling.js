import React, { Component } from 'react';

export default class EventHandling extends Component {
  isLogin = true;
  //   conditional rendering
  renderContent = () => {
    if (this.isLogin == false) {
      return (
        <button onClick={this.handleLogin} className="btn btn-success">
          Login
        </button>
      );
    } else {
      return (
        <button onClick={this.handleLogout} className="btn btn-danger">
          Logout
        </button>
      );
    }
  };
  handleLogin = () => {
    this.isLogin = true;
    console.log(`  this.isLogin`, this.isLogin);
  };
  handleLogout = () => {
    this.isLogin = false;
    console.log(`  this.isLogin`, this.isLogin);
  };

  hanleSayHelloWithName = (username) => {
    console.log('hello', username);
  };
  render() {
    console.log('render', this.isLogin);
    return (
      <div>
        {/* render button */}
        <div>{this.renderContent()}</div>
        <butotn
          className="btn btn-warning"
          //   dùng arrow function đối với hàm có tham số
          onClick={() => {
            this.hanleSayHelloWithName('Alice');
          }}
        >
          Say hello with name
        </butotn>
      </div>
    );
  }
}
