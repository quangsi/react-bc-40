import React, { Component } from 'react';

export default class UserInfo extends Component {
  render() {
    console.log('this.props', this.props);
    return (
      <div>
        <h2>UserInfo</h2>
        <h3>Username: {this.props.username} </h3>
        <h3>Age: {this.props.age} </h3>
        <button onClick={this.props.handleOnclick} className="btn btn-warning">
          Change username
        </button>
      </div>
    );
  }
}
