import React, { Component } from 'react';
import UserInfo from './UserInfo';
// DemoProp: Component cha
// UserInfo: Component
export default class DemoProp extends Component {
  state = {
    name: 'alice',
  };
  handleChangeUsername = () => {
    // state ở đâu, setState tại đó
    this.setState({ name: 'bob' });
  };
  render() {
    return (
      <div>
        <h2>DemoProp</h2>
        <UserInfo
          handleOnclick={this.handleChangeUsername}
          age="2"
          username={this.state.name}
        />
      </div>
    );
  }
}

let a = 2;
let b = a;
