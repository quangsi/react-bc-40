import React, { Component } from 'react';

export default class Ex_Car_Color extends Component {
  state = {
    img_url: './img_source/CarBasic/products/red-car.jpg',
  };
  handleChangeColorCar = (color) => {
    let src = `./img_source/CarBasic/products/${color}-car.jpg`;
    this.setState({ img_url: src });
  };
  render() {
    return (
      <div>
        <h2 className="mb-5">Ex_Car_Color</h2>
        <div className="row">
          <img className="col-4" src={this.state.img_url} alt="" />
          <div>
            <button
              onClick={() => {
                this.handleChangeColorCar({
                  color: 'red',
                });
              }}
              className="btn btn-danger"
            >
              Red
            </button>
            <button
              onClick={() => {
                this.handleChangeColorCar('black');
              }}
              className="btn btn-dark mx-5"
            >
              Black
            </button>
            <button
              onClick={() => {
                this.handleChangeColorCar('silver');
              }}
              className="btn btn-secondary"
            >
              Silver
            </button>
          </div>
        </div>
      </div>
    );
  }
}
