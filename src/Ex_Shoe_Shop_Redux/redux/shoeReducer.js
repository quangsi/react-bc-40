import { dataShoe } from "../data_shoe";
import * as types from "./constants/shoeContants";
let initialValue = {
  listShoe: dataShoe,
  cart: [],
};

export const shoeReducer = (state = initialValue, action) => {
  switch (action.type) {
    case types.ADD_TO_CART: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => {
        return item.id == action.payload.id;
      });
      if (index == -1) {
        let newShoe = { ...action.payload, soLuong: 1 };
        cloneCart.push(newShoe);
      } else {
        cloneCart[index].soLuong++;
      }
      return { ...state, cart: cloneCart };
    }

    case types.DELTETE_ITEM: {
      // splice
      //
      let newCart = state.cart.filter((item) => {
        return item.id != action.payload;
      });
      return { ...state, cart: newCart };
    }
    default:
      return state;
  }
};
// rootReducer_Ex_Shoe_Shop_Redux
// filter js
