import React, { Component } from 'react';

export default class DemoState extends Component {
  // *** state: quản lý các giá trị ảnh hưởng đến giao diện (update giá trị thì render lại giao diện)
  state = {
    username: 'Alice',
  };
  //   setState : sinh ra dùng để update giá trị của state
  handleChangeUsername = () => {
    // let name = null;
    // if (this.state.username == 'Bob') {
    //   name = 'Alice';
    // } else {
    //   name = 'Bob';
    // }

    let name = this.state.username == 'Bob' ? 'Alice' : 'Bob';
    this.setState({
      username: name,
    });
  };
  render() {
    return (
      <div>
        <h2
          className={
            this.state.username == 'Bob' ? 'text-danger' : 'text-secondary'
          }
        >
          Username : {this.state.username}
        </h2>
        <button onClick={this.handleChangeUsername} className="btn btn-success">
          Change username
        </button>
      </div>
    );
  }
}
