import React, { Component } from 'react';

export default class RenderingWithMap extends Component {
  state = {
    foodList: [
      {
        tenMon: 'Cơm tấm ddasdasd',
        giaMon: '873.00244785785',
        hinhAnh: 'https://loremflickr.com/640/480',
        loaiMon: 'Chay',
        maMon: '68',
      },
      {
        tenMon: 'Crucian carp',
        giaMon: '369.00',
        hinhAnh: 'https://loremflickr.com/640/480',
        loaiMon: 'Mặn',
        maMon: '70',
      },
      {
        tenMon: 'Madeiran sardinella',
        giaMon: '643.00',
        hinhAnh: 'https://loremflickr.com/640/480',
        loaiMon: true,
        maMon: '71',
      },
      {
        tenMon: 'Silver cyprinid',
        giaMon: '845.00',
        hinhAnh: 'https://loremflickr.com/640/480',
        loaiMon: false,
        maMon: '72',
      },
      {
        tenMon: 'Round sardinella',
        giaMon: '112.00',
        hinhAnh: 'https://loremflickr.com/640/480',
        loaiMon: false,
        maMon: '73',
      },
      {
        tenMon: 'Crucian carp',
        giaMon: '200.00',
        hinhAnh: 'https://loremflickr.com/640/480',
        loaiMon: true,
        maMon: '74',
      },
      {
        tenMon: 'Argentine hake',
        giaMon: '999.00',
        hinhAnh: 'https://loremflickr.com/640/480',
        loaiMon: true,
        maMon: '75',
      },
      {
        tenMon: 'Northern snakehead',
        giaMon: '10.00',
        hinhAnh: 'https://loremflickr.com/640/480',
        loaiMon: true,
        maMon: '77',
      },
      {
        tenMon: 'Cá',
        giaMon: '50000',
        hinhAnh: '',
        loaiMon: 'Chay',
        maMon: '78',
      },
      {
        tenMon: 'Cá',
        giaMon: '50000',
        hinhAnh: '',
        loaiMon: 'Chay',
        maMon: '79',
      },
      {
        tenMon: 'bán mì',
        giaMon: '23',
        hinhAnh: 'lorem.img',
        loaiMon: 'Mặn',
        maMon: '80',
      },
      {
        tenMon: 'bánhhhhhhhhh mì',
        giaMon: '23',
        hinhAnh: 'lorem.img',
        loaiMon: 'Mặn',
        maMon: '81',
      },
      {
        tenMon: 'Hamburger',
        giaMon: '5999',
        hinhAnh: '',
        loaiMon: 'Mặn',
        maMon: '82',
      },
    ],
  };
  renderFoodList = () => {
    let foodListComponents = this.state.foodList.slice(0, 8).map((food) => {
      return (
        <div className="card col-3" style={{ width: '18rem' }}>
          <div className="card-body">
            <h5 className="card-title">{food.tenMon}</h5>
            <p className="card-text">{food.giaMon}</p>
            <a href="#" className="btn btn-primary">
              thêm vàog giỏ hàng
            </a>
          </div>
        </div>
      );
    });
    return foodListComponents;
    // map()
  };
  render() {
    return (
      <div>
        <h2>RenderingWithMap</h2>
        <div className="row">{this.renderFoodList()}</div>
      </div>
    );
  }
}
