let initialValue = {
  number: 100,
};

export const numberReducer = (state = initialValue, action) => {
  switch (action.type) {
    case "TANG_SO_LUONG": {
      state.number = state.number + 10;
      return { ...state };
    }
    case "GIAM_SO_LUONG": {
      console.log("aciton ", action);
      state.number = state.number - action.payload;
      return { ...state };
    }
    default: {
      return state;
    }
  }
};
