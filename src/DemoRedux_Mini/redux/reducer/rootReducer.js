import { combineReducers } from "redux";
import { numberReducer } from "./numberReducer";

export const rootReducer_DemoReduxMini = combineReducers({
  numberReducer: numberReducer,
});

// key: value
// key: đại diện cho giá trị state trong reducer
// value : tên của các reducer
