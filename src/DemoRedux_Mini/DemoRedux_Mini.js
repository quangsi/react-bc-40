import React, { Component } from "react";
import { connect } from "react-redux";

class DemoRedux_Mini extends Component {
  render() {
    console.log(this.props);
    return (
      <div>
        <button
          className="btn btn-danger"
          onClick={() => {
            this.props.handleGiamSoLuong();
          }}
        >
          -
        </button>
        <strong className="mx-5">{this.props.soLuong}</strong>
        <button
          onClick={this.props.handleTangSoLuong}
          className="btn btn-success"
        >
          +
        </button>
      </div>
    );
  }
}

// lấy data từ state của REDUX

let mapStateToProps = (state) => {
  return {
    soLuong: state.numberReducer.number,
  };
  // key : ten props
  // value : giá trị của state REDUX
};
let mapDispatchToProps = (dispatch) => {
  return {
    handleTangSoLuong: () => {
      let action = {
        type: "TANG_SO_LUONG",
      };
      dispatch(action);
    },
    handleGiamSoLuong: () => {
      dispatch({
        type: "GIAM_SO_LUONG",
        payload: 5,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DemoRedux_Mini);
